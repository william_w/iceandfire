import React from 'react';
import { Route, Switch} from 'react-router-dom';

import routes from "../routes";

export default () => (
	<Switch>
		{routes.map((route, index) => {
			return (
				<Route
					key={index}
					path={route.path}
					exact={route.exact}
					component={props => {
						return (
							route.layout ? (
								<route.layout>
									<route.component {...props}/>
								</route.layout>
							) : (
								<route.component {...props}/>
							)
						);
					}}
				/>
			);
		})}
	</Switch>
);