import React, {useState, useEffect} from 'react';
import history from '../../history';
import { makeStyles } from '@material-ui/core/styles';

import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import BooksIcon from '@material-ui/icons/LibraryBooksTwoTone';
import CharactersIcon from '@material-ui/icons/FaceTwoTone';
import HousesIcon from '@material-ui/icons/SecurityTwoTone';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    color: theme.palette.text.secondary,
    height: '100%'
  },
  menuItem: {
    color: theme.palette.text.secondary,
    height: 50
  },
  menuItemFocused: {
    color: "#1a73e8",
    backgroundColor: '#f5f6f8',
    height: 50
  }
}));

const SideBar = ({name}) => {
  const classes = useStyles();
  const [state, setState] = useState({
    books: false,
    houses: false,
    characters: false,
  });

  useEffect(() => {
    if(name.includes('Book'))
      setState({books: true});
    else if(name.includes('House'))
      setState({houses: true});
    else if(name.includes('Character'))
      setState({characters: true});
  }, [name])

  const onClickMenu = (item) => {
    history.push(`/${item}`);
  }

  const {books, houses, characters} = state;
  return (
    <div className={classes.root}>
      <MenuList>
        <MenuItem onClick={() => onClickMenu('books')} className={books ? classes.menuItemFocused : classes.menuItem} >
          <ListItemIcon>
            <BooksIcon fontSize="small" className={books ? classes.menuItemFocused : classes.menuItem} />
          </ListItemIcon>
          <Typography variant="inherit" noWrap>Books</Typography>
        </MenuItem>

        <MenuItem onClick={() => onClickMenu('houses')} className={houses ? classes.menuItemFocused : classes.menuItem}>
          <ListItemIcon>
            <HousesIcon fontSize="small" className={houses ? classes.menuItemFocused : classes.menuItem} />
          </ListItemIcon>
          <Typography variant="inherit" noWrap>Houses</Typography>
        </MenuItem>

        <MenuItem onClick={() => onClickMenu('characters')} className={characters ? classes.menuItemFocused : classes.menuItem}>
          <ListItemIcon>
            <CharactersIcon fontSize="small" className={characters ? classes.menuItemFocused : classes.menuItem} />
          </ListItemIcon>
          <Typography variant="inherit" noWrap>Characters</Typography>
        </MenuItem>
      </MenuList>
    </div>
  );
}

export default SideBar;