import React, {useState, useEffect} from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import cyan from '@material-ui/core/colors/cyan';
import green from '@material-ui/core/colors/green';

import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import Paper from '@material-ui/core/Paper';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: cyan[200],
      main: 'rgb(0, 188, 212)',
      dark: cyan[700],
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700],
    },
  },
});

const Characters = props => {
  const {characters, page, setCharacter, setPage, end, setEnd} = props;
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    setCharacter(characters[0]);
    setSelected([characters[0].url]);
  }, [characters]);

  const handleClick = (event, url) => {
    let newSelected = [];
    newSelected = newSelected.concat([], url);
    const character = characters.find(character => character.url === url)
    setCharacter(character);
    setSelected(newSelected);
  };

  const previous = (e) => {
    e.preventDefault();
    end ? setPage(page - 2) : setPage(page - 1);
    setEnd(false);
  }

  const next = (e) => {
    e.preventDefault();
    setPage(page + 1);
  }

  const isSelected = url => selected.indexOf(url) !== -1;

  return (
    <ThemeProvider theme={theme}>
      <Typography variant="overline" display="block" color="textSecondary" gutterBottom noWrap>
        Book List
      </Typography>
      <Paper elevation={5}>
        <TableContainer>
          <Table aria-label="characters table">
            <TableHead>
              <TableRow>
                <TableCell><b>Name</b></TableCell>
                {/* <TableCell><b>ISBN</b></TableCell> */}
              </TableRow>
            </TableHead>
            <TableBody>
              {characters.map((row, index) => {
                const isItemSelected = isSelected(row.url);
                return (
                  <TableRow 
                    key={index}
                    hover
                    onClick={event => handleClick(event, row.url)}
                    selected={isItemSelected}
                  >
                    <TableCell component="th" scope="row">
                      {row.name !== '' ? row.name : row.aliases[0]}
                    </TableCell>
                    {/* <TableCell>{row.url}</TableCell> */}
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
          <span>
            <IconButton aria-label="previous" disabled={page > 1 ? false : true} onClick={(e) => previous(e)}>
              <NavigateBeforeIcon />
            </IconButton>
            <IconButton aria-label="next" disabled={end} onClick={(e) => next(e)}>
              <NavigateNextIcon />
            </IconButton>
          </span>
        </div>
      </Paper>
    </ThemeProvider>
  );
}

export default Characters;