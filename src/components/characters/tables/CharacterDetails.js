import React from 'react';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { Link } from 'react-router-dom';

import cyan from '@material-ui/core/colors/cyan';
import green from '@material-ui/core/colors/green';

import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: { flexGrow: 1, },
  list: { height: 20, },
}));

const theme = createMuiTheme({
  palette: {
    primary: {
      light: cyan[200],
      main: 'rgb(0, 188, 212)',
      dark: cyan[700],
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700],
      contrastText: "white"
    },
  },
});

const CharacterDetails = ({character}) => {
  const classes = useStyles();

  const {url, name, gender, culture, born, died, titles, aliases, father, mother, spouse, allegiances, books, povBooks, tvSeries, playedBy} = character;

  const titleList = titles.map(title => {
    return <ListItem key={title} className={classes.list} >{title}</ListItem>;
  });

  const aliasList = aliases.map(alias => {
    return <ListItem key={alias} className={classes.list} >{alias}</ListItem>;
  });

  const allegianceList = allegiances.map(allegiance => {
    return (
      <ListItem key={allegiance} className={classes.list} >
        {allegiance}
      </ListItem>
    );
  });

  const bookList = books.map(book => {
    return (
      <ListItem key={book} className={classes.list} >
        {book}
      </ListItem>
    );
  });

  const povBookList = povBooks.map(povBook => {
    return (
      <ListItem key={povBook} className={classes.list} >
        {povBook}
      </ListItem>
    );
  });

  const tvSeriesList = tvSeries.map(tvSerie => {
    return (
      <ListItem key={tvSerie} className={classes.list} >
        {tvSerie}
      </ListItem>
    );
  });

  const playedByList = playedBy.map(plydBy => {
    return (
      <ListItem key={plydBy} className={classes.list} >
        {plydBy}
      </ListItem>
    );
  });

  const stripId = url => {
    return url.substring(url.lastIndexOf('/') + 1, url.length);
  }
  const fatherId = stripId(father);
  const motherId = stripId(mother);
  const spouseId = stripId(spouse);
  
  return (
    <ThemeProvider theme={theme}>
      <Typography variant="overline" display="block" color="textSecondary" gutterBottom noWrap>
        Character Details
      </Typography>
      <Paper elevation={5}>
        <TableContainer>
          <Table aria-label="spanning table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        URL:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Name:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Gender:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Culture:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Born:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Died:
                      </Typography>
                    </ListItem>
                  </List>
                </TableCell>
                <TableCell>
                  <List>
                    <ListItem className={classes.list}>{url}</ListItem>
                    <ListItem className={classes.list}>{name}</ListItem>
                    <ListItem className={classes.list}>{gender}</ListItem>
                    <ListItem className={classes.list}>{culture}</ListItem>
                    <ListItem className={classes.list}>{born}</ListItem>
                    <ListItem className={classes.list}>{died}</ListItem>
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell colSpan={2}>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Titles:
                      </Typography>
                    </ListItem>
                    {titleList}
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Aliases:
                      </Typography>
                    </ListItem>
                    {aliasList}
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Father:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Mother:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Spouse:
                      </Typography>
                    </ListItem>
                  </List>
                </TableCell>
                <TableCell>
                  <List>
                    <ListItem className={classes.list}>
                      <Link target="_blank" to={`/character/${fatherId}`}>
                        {father}
                      </Link>
                    </ListItem>
                    <ListItem className={classes.list}>
                      <Link target="_blank" to={`/character/${motherId}`}>
                        {mother}
                      </Link>
                    </ListItem>
                    <ListItem className={classes.list}>
                      <Link target="_blank" to={`/character/${spouseId}`}>
                        {spouse}
                      </Link>
                    </ListItem>
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell colSpan={2}>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Allegiances:
                      </Typography>
                    </ListItem>
                    {allegianceList}
                    <br />
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Books:
                      </Typography>
                    </ListItem>
                    {bookList}
                    <br />
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        POV Books:
                      </Typography>
                    </ListItem>
                    {povBookList}
                    <br />
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        TV Series:
                      </Typography>
                    </ListItem>
                    {tvSeriesList}
                    <br />
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Played By:
                      </Typography>
                    </ListItem>
                    {playedByList}
                  </List>
                </TableCell>
              </TableRow>
            </TableHead>
          </Table>
        </TableContainer>
      </Paper>
    </ThemeProvider>
  )
}

export default CharacterDetails;