import React, { useState, useEffect } from 'react';
import CharacterDetails from './tables/CharacterDetails';

const LoadCharacter = props => {
  const [hasError, setErrors] = useState(false);
  const [character, setCharacter] = useState(null);

  const fetchData = async (id) => {
    const response = await fetch(`https://www.anapioficeandfire.com/api/characters/${id}`);
    response
    .json()
    .then(response => setCharacter(response))
    .catch(() => setErrors(true));
  };

  useEffect(() => {
    const id = props.match ? props.match.params.id : null;
    id && fetchData(id);
  }, []);

  if (hasError) {
    return <div>Loading...</div>;
  }

  return character && (
    <div style={{margin: 15}}>
		  <CharacterDetails character={character} />
    </div>
  );
}

export default LoadCharacter;