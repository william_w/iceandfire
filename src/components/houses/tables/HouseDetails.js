import React, {useState, useEffect, Fragment} from 'react';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { Link } from 'react-router-dom';

import cyan from '@material-ui/core/colors/cyan';
import green from '@material-ui/core/colors/green';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TablePagination from '@material-ui/core/TablePagination';

const useStyles = makeStyles(theme => ({
  root: { flexGrow: 1, },
  list: { height: 20, maxWidth: 450},
}));

const theme = createMuiTheme({
  palette: {
    primary: {
      light: cyan[200],
      main: 'rgb(0, 188, 212)',
      dark: cyan[700],
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700],
      contrastText: "white"
    },
  },
});

const HouseDetails = ({house, page}) => {
  const classes = useStyles();
  const {url, name, region, coatOfArms, words, titles, seats, currentLord, heir, overlord, founded, founder, diedOut, ancestralWeapons, cadetBranches, swornMembers} = house;
  
  const [swornMemberPage, setSwornMemberPage] = useState(0);

  useEffect(() => {
    setSwornMemberPage(0);
  }, [page, house]);

  const handleSwornMemberChangePage = (event, newPage) => {
    setSwornMemberPage(newPage);
  };

  const titlesList = titles.map(title => {
    return <ListItem key={title} className={classes.list} >{title}</ListItem>;
  });

  const seatsList = seats.map(seat => {
    return <ListItem key={seat} className={classes.list} >{seat}</ListItem>;
  });

  const ancestralWeaponsList = ancestralWeapons.map(ancestralWeapon => {
    return <ListItem key={ancestralWeapon} className={classes.list} >{ancestralWeapon}</ListItem>;
  });

  const cadetBranchesList = cadetBranches.map(cadetBranch => {
    return <ListItem key={cadetBranch} className={classes.list} >{cadetBranch}</ListItem>;
  });

  const stripId = url => {
    return url.substring(url.lastIndexOf('/') + 1, url.length);
  };

  const currentLordId = stripId(currentLord);
  let swornMemberId = null;

  const swornMembersList = () => {
    return (
      <Fragment>
        <TableContainer>
          <Table aria-label="characters table">
            <TableBody>
              {swornMembers.slice(swornMemberPage * 5, swornMemberPage * 5 + 5)
              .map((swornMember, index) => {
                // return link elements for swornMembers
                swornMemberId = stripId(swornMember);
                return (
                  <TableRow key={index} hover>
                    <TableCell scope="row">
                      <Link target="_blank" to={`/character/${swornMemberId}`}>
                        {swornMember}
                      </Link>
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[]}
          component="div"
          count={swornMembers.length}
          rowsPerPage={5}
          page={swornMemberPage}
          onChangePage={handleSwornMemberChangePage}
          style={{width:'250px'}}
        />
      </Fragment>
    );
  };
 
  return (
    <ThemeProvider theme={theme}>
      <Typography variant="overline" display="block" color="textSecondary" gutterBottom noWrap>
        House Details
      </Typography>
      <Paper elevation={5}>
        <TableContainer>
          <Table aria-label="spanning table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        URL:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Name:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Region:
                      </Typography>
                    </ListItem>
                    < br />
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Coat of Arms:
                      </Typography>
                    </ListItem>
                    < br />
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Words:
                      </Typography>
                    </ListItem>
                  </List>
                </TableCell>
                <TableCell>
                  <List>
                    <ListItem className={classes.list}>{url}</ListItem>
                    <ListItem className={classes.list}>{name}</ListItem>
                    <ListItem className={classes.list}>{region}</ListItem>
                    < br />
                    <ListItem className={classes.list}>{coatOfArms}</ListItem>
                    < br />
                    <ListItem className={classes.list}>{words}</ListItem>
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell colSpan={2}>
                  <List>
                  <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Titles:
                      </Typography>
                    </ListItem>
                    {titlesList}
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Seats:
                      </Typography>
                    </ListItem>
                    {seatsList}
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Current Lord:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Heir:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Overlord:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Founded:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Founder:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Died Out:
                      </Typography>
                    </ListItem>
                  </List>
                </TableCell>
                <TableCell>
                  <List>
                    <ListItem className={classes.list}>
                      <Link target="_blank" to={`/character/${stripId(currentLord)}`}>
                        {currentLord}
                      </Link>
                    </ListItem>
                    <ListItem className={classes.list}>
                      <Link target="_blank" to={`/character/${stripId(heir)}`}>
                        {heir}
                      </Link>
                    </ListItem>
                    <ListItem className={classes.list}>{overlord}</ListItem>
                    <ListItem className={classes.list}>{founded}</ListItem>
                    <ListItem className={classes.list}>
                      <Link target="_blank" to={`/character/${stripId(founder)}`}>
                        {founder}
                      </Link>
                    </ListItem>
                    <ListItem className={classes.list}>{diedOut}</ListItem>
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell colSpan={2}>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Ancestral Weapons:
                      </Typography>
                    </ListItem>
                    {ancestralWeaponsList}
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Cadet Branches:
                      </Typography>
                    </ListItem>
                    {cadetBranchesList}
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell colSpan={2}>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Sworn Members:
                      </Typography>
                    </ListItem>
                  </List>
                  {swornMembersList()}
                </TableCell>
              </TableRow>
            </TableHead>
          </Table>
        </TableContainer>
      </Paper>
    </ThemeProvider>
  )
}

export default HouseDetails;