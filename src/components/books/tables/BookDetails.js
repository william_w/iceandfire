import React, {useState, useEffect, Fragment} from 'react';
import { Link } from 'react-router-dom';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import cyan from '@material-ui/core/colors/cyan';
import green from '@material-ui/core/colors/green';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TablePagination from '@material-ui/core/TablePagination';

const useStyles = makeStyles(theme => ({
  root: { flexGrow: 1, },
  list: { height: 20, },
}));

const theme = createMuiTheme({
  palette: {
    primary: {
      light: cyan[200],
      main: 'rgb(0, 188, 212)',
      dark: cyan[700],
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700],
      contrastText: "white"
    },
  },
});

const BookDetails = ({book, page}) => {
  const classes = useStyles();
  const {url, name, isbn, authors, numberOfPages, publisher, country, mediaType, released, characters, povCharacters} = book;
  
  const [characterPage, setCharacterPage] = useState(0);
  const [povCharacterPage, setPovCharacterPage] = useState(0);

  useEffect(() => {
    setCharacterPage(0);
    setPovCharacterPage(0);
  }, [page, book]);

  const handlePovCharacterChangePage = (event, newPage) => {
    setPovCharacterPage(newPage);
  };

  const handleCharacterChangePage = (event, newPage) => {
    setCharacterPage(newPage);
  };

  const authorsList = authors.map(author => {
    return <ListItem key={author} className={classes.list} >{author}</ListItem>;
  });

  const stripId = url => {
    return url.substring(url.lastIndexOf('/') + 1, url.length);
  };
  const charactersList = () => {
    return (
      <Fragment>
        <TableContainer>
          <Table aria-label="characters table">
            <TableBody>
              {characters.slice(characterPage * 5, characterPage * 5 + 5)
              .map((character, index) => {
                // return link elements for characters
                return (
                  <TableRow key={index} hover>
                    <TableCell scope="row">
                      <Link target="_blank" to={`/character/${stripId(character)}`}>
                        {character}
                      </Link>
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[]}
          component="div"
          count={characters.length}
          rowsPerPage={5}
          page={characterPage}
          onChangePage={handleCharacterChangePage}
          style={{width:'250px'}}
        />
      </Fragment>
    );
  };

  const povCharactersList = () => {
    return (
      <Fragment>
        <TableContainer>
          <Table aria-label="povCharacters table">
            <TableBody>
              {povCharacters.slice(povCharacterPage * 5, povCharacterPage * 5 + 5)
              .map((povCharacter, index) => {
                  return (
                    <TableRow key={index} hover>
                      <TableCell component="th" scope="row">
                        <Link target="_blank" to={`/character/${stripId(povCharacter)}`}>
                          {povCharacter}
                        </Link>
                      </TableCell>
                    </TableRow>
                  )
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[]}
          component="div"
          count={povCharacters.length}
          rowsPerPage={5}
          page={povCharacterPage}
          onChangePage={handlePovCharacterChangePage}
          style={{width:'250px'}}
        />
      </Fragment>
    )
  };
  
  return (
    <ThemeProvider theme={theme}>
      <Typography variant="overline" display="block" color="textSecondary" gutterBottom noWrap>
        Book Details
      </Typography>
      <Paper elevation={5}>
        <TableContainer>
          <Table aria-label="spanning table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        URL:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Name:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        ISBN:
                      </Typography>
                    </ListItem>
                  </List>
                </TableCell>
                <TableCell>
                  <List>
                    <ListItem className={classes.list}>{url}</ListItem>
                    <ListItem className={classes.list}>{name}</ListItem>
                    <ListItem className={classes.list}>{isbn}</ListItem>
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell colSpan={2}>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Authors:
                      </Typography>
                    </ListItem>
                    {authorsList}
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Pages:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Publisher:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Country:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Media type:
                      </Typography>
                    </ListItem>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Released:
                      </Typography>
                    </ListItem>
                  </List>
                </TableCell>
                <TableCell>
                  <List>
                    <ListItem className={classes.list}>{numberOfPages}</ListItem>
                    <ListItem className={classes.list}>{publisher}</ListItem>
                    <ListItem className={classes.list}>{country}</ListItem>
                    <ListItem className={classes.list}>{mediaType}</ListItem>
                    <ListItem className={classes.list}>{released}</ListItem>
                  </List>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell colSpan={2}>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        Characters:
                      </Typography>
                    </ListItem>
                  </List>
                  {charactersList()}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell colSpan={2}>
                  <List>
                    <ListItem className={classes.list} >
                      <Typography variant="overline" gutterBottom type-face='nunito'>
                        POV Characters:
                      </Typography>
                    </ListItem>
                  </List>
                  {povCharactersList()}
                </TableCell>
              </TableRow>
            </TableHead>
          </Table>
        </TableContainer>
      </Paper>
    </ThemeProvider>
  )
}

export default BookDetails;