import React from 'react';
import { Redirect } from 'react-router-dom';

// Default page layout
import {DefaultLayout} from './layout';

// Containers
import {BookList} from './containers/BookList';
import CharacterList from './containers/CharacterList';
import HousesList from './containers/HousesList';

//Components
import LoadCharacter from './components/characters/LoadCharacter';

export default [
  {
    path: '/',
    exact: true,
    component: () => <Redirect to='/books' />,
  },
  {
    path: '/books',
    exact: true,
    layout: DefaultLayout,
    component: BookList,
  },
  {
    path: '/characters',
    exact: true,
    layout: DefaultLayout,
    component: CharacterList,
  },
  {
    path: '/character/:id',
    exact: true,
    component: LoadCharacter,
  },
  {
    path: '/houses',
    exact: true,
    layout: DefaultLayout,
    component: HousesList,
  },
];