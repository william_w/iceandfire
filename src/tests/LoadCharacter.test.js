import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';
import { MemoryRouter as Router } from 'react-router-dom';

import LoadCharacter from "../components/characters/LoadCharacter";

describe('LoadCharacter component', () => {

  test('renders correctly', () => {
    const tree = renderer
      .create(
        <Router>
          <LoadCharacter />
        </Router>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});