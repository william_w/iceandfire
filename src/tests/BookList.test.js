import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';

import { BookList } from "../containers/BookList";

describe('BookList container', () => {

  test('renders correctly', () => {
    const tree = renderer
      .create(<BookList />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});