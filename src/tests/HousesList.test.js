import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';

import HousesList from "../containers/HousesList";

describe('HousesList container', () => {

  test('renders correctly', () => {
    const tree = renderer
      .create(<HousesList />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});