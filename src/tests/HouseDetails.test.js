import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';
import { MemoryRouter as Router } from 'react-router-dom';

import HouseDetails from '../components/houses/tables/HouseDetails';
import {house} from './__mocks__/house';

function setup() {
  const props = {
    house,
    page: 1
  }

  return {
    props
  }
}

describe('HouseDetails component', () => {

  test('renders correctly', () => {
    const { props } = setup();
    const tree = renderer
      .create(
      <Router>
        <HouseDetails {...props}/>
      </Router>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});