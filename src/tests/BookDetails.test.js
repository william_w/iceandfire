import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';
import { MemoryRouter as Router } from 'react-router-dom';

import BookDetails from '../components/books/tables/BookDetails';
import {book} from './__mocks__/book';

function setup() {
  const props = {
    book,
    page: 1
  }

  return {
    props
  }
}

describe('BookDetails component', () => {

  test('renders correctly', () => {
    const { props } = setup();
    const tree = renderer
      .create(
        <Router>
          <BookDetails {...props}/>
        </Router>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});