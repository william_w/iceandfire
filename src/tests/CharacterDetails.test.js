import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';
import { MemoryRouter as Router } from 'react-router-dom';

import CharacterDetails from '../components/characters/tables/CharacterDetails';
import {character} from './__mocks__/character';

function setup() {
  const props = {
    character
  }

  return {
    props
  }
}

describe('CharacterDetails component', () => {

  test('renders correctly', () => {
    const { props } = setup();
    const tree = renderer
      .create(
        <Router>
          <CharacterDetails {...props}/>
        </Router>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});