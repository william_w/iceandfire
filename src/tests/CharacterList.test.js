import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';

import CharacterList from "../containers/CharacterList";

describe('CharacterList container', () => {

  test('renders correctly', () => {
    const tree = renderer
      .create(<CharacterList />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});