import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';

import Books from '../components/books/tables/Books';
import {books} from './__mocks__/books';

function setup() {
  const props = {
    books
  }

  return {
    props
  }
}

describe('Books component', () => {

  test('renders correctly', () => {
    const { props } = setup();
    const tree = renderer
      .create(<Books {...props}/>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});