import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';

import Characters from '../components/characters/tables/Characters';
import {characters} from './__mocks__/characters';

function setup() {
  const props = {
    characters
  }

  return {
    props
  }
}

describe('Characters component', () => {

  test('renders correctly', () => {
    const { props } = setup();
    const tree = renderer
      .create(<Characters {...props}/>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});