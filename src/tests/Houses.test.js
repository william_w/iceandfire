import React from 'react';
import renderer from 'react-test-renderer';
import expect from 'expect';

import Houses from '../components/houses/tables/Houses';
import {houses} from './__mocks__/houses';

function setup() {
  const props = {
    houses
  }

  return {
    props
  }
}

describe('Houses component', () => {

  test('renders correctly', () => {
    const { props } = setup();
    const tree = renderer
      .create(<Houses {...props}/>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});