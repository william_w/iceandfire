import React, { useState, useEffect } from 'react';
import Characters from '../components/characters/tables/Characters';
import CharacterDetails from '../components/characters/tables/CharacterDetails';

import Grid from '@material-ui/core/Grid';

const CharacterList = () => {
  const [hasError, setErrors] = useState(false);
  const [characters, setCharacters] = useState([]);
  const [page, setPage] = useState(1);
  const [end, setEnd] = useState(false);
  const [character, setCharacter] = useState(null);
  const [pageSize, ] = useState(5);

  const fetchData = async (page, pageSize) => {
    const response = await fetch(`https://www.anapioficeandfire.com/api/characters?page=${page}&pageSize=${pageSize}`);
    response
    .json()
    .then(response => {
      response.length > 0 ? setCharacters(response) : setEndOfList()
    })
    .catch(() => setErrors(true));
  };
  
  useEffect(() => {
    fetchData(page, pageSize);
  }, [page, pageSize]);

  const setEndOfList = () => {
    setEnd(true);
  };

  if (hasError) {
    return <div>Loading...</div>;
  }

  const props = {characters, setCharacter, page, setPage, end, setEnd};
  return (
    <div>
      <Grid container spacing={2}>
				<Grid item xs={4}>
					{characters.length > 0 && (
						<Characters {...props} />
					)}
				</Grid>
				<Grid item xs={8}>
					{ character && (
						<CharacterDetails character={character} />
					)}
				</Grid>
			</Grid>
    </div>
  );
}

export default CharacterList;