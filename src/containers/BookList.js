import React, { useState, useEffect } from 'react';
import Books from '../components/books/tables/Books';
import BookDetails from '../components/books/tables/BookDetails';

import Grid from '@material-ui/core/Grid';

export const BookList = () => {
  const [hasError, setErrors] = useState(false);
  const [books, setBooks] = useState([]);
  const [page, setPage] = useState(1);
  const [end, setEnd] = useState(false);
  const [book, setBook] = useState(null);
  const [pageSize, ] = useState(5);

  const fetchData = async (page, pageSize) => {
    const response = await fetch(`https://www.anapioficeandfire.com/api/books?page=${page}&pageSize=${pageSize}`);
    response
    .json()
    .then(response => {
      response.length > 0 ? setBooks(response) : setEndOfList()
    })
    .catch(() => setErrors(true));
  };
  
  useEffect(() => {
    fetchData(page, pageSize);
  }, [page, pageSize]);

  const setEndOfList = () => {
    setEnd(true);
  };

  if (hasError) {
    return <div>Loading...</div>;
  }

  const props = {books, setBook, page, setPage, end, setEnd};
  return (
    <div>
      <Grid container spacing={2}>
				<Grid item xs={4}>
					{books.length > 0 && (
						<Books {...props} />
					)}
				</Grid>
				<Grid item xs={8}>
					{ book && (
						<BookDetails book={book} page={page} />
					)}
				</Grid>
			</Grid>
    </div>
  );
}