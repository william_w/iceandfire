import React, { useState, useEffect } from 'react';
import Houses from '../components/houses/tables/Houses';
import HouseDetails from '../components/houses/tables/HouseDetails';

import Grid from '@material-ui/core/Grid';

const HouseList = () => {
  const [hasError, setErrors] = useState(false);
  const [houses, setHouses] = useState([]);
  const [page, setPage] = useState(1);
  const [end, setEnd] = useState(false);
  const [house, setHouse] = useState(null);
  const [pageSize, ] = useState(5);

  const fetchData = async (page, pageSize) => {
    const response = await fetch(`https://www.anapioficeandfire.com/api/houses?page=${page}&pageSize=${pageSize}`);
    response
    .json()
    .then(response => {
      response.length > 0 ? setHouses(response) : setEndOfList()
    })
    .catch(() => setErrors(true));
  };
  
  useEffect(() => {
    fetchData(page, pageSize);
  }, [page, pageSize]);

  const setEndOfList = () => {
    setEnd(true);
  };

  if (hasError) {
    return <div>Loading...</div>;
  }

  const props = {houses, setHouse, page, setPage, end, setEnd};
  return (
    <div>
      <Grid container spacing={2}>
				<Grid item xs={4}>
					{houses.length > 0 && (
						<Houses {...props} />
					)}
				</Grid>
				<Grid item xs={8}>
					{ house && (
						<HouseDetails house={house} page={page} />
					)}
				</Grid>
			</Grid>
    </div>
  );
}

export default HouseList;