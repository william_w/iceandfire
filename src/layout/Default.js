import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import SideBar from '../components/layout/SideBar';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#f5f6f8',
    margin: 0,
  },
  grid: {
    height: '100%'
  },
  raised: {
    zIndex: 20
  },
  paper: {
    width: '17%',
    position: 'fixed',
    height: '100%'
  },
  gridItem: {
    marginTop: 0,
    marginLeft: 25,
    marginBottom: 20,
    marginRight: 15,
  }
}));

const DefaultLayout = ({children}) => {
  const classes = useStyles();
  const name = children.type.name ? children.type.name : children.type.displayName;
  return (
    <div className={classes.root}>
      <Grid container direction="row" spacing={0} className={classes.grid}>
        <Grid item xs={2} className={classes.raised} >
          <Grid container direction="column" spacing={0} className={classes.grid}>
            <Paper elevation={3} square className={classes.paper}>
              <Grid item>
                <SideBar name={name} />
              </Grid>
            </Paper>
          </Grid>
        </Grid>
        <Grid item xs={10}>
          <Grid container direction="column" spacing={0}>
            <Grid item>
              <div className={classes.gridItem} >
                {children}
              </div>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default DefaultLayout;