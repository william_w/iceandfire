# iceandfire

Front-end app that displays a list (and details) of books, characters, and house from the ice & fire (https://www.anapioficeandfire.com/api/) api.

## Pre-requisites
You will need to have the following installed and configured appropriately on your computer.
* Nodejs
* Git
* yarn


## Setup:

### Clone the project to your local drive
1. Navigate to your projects folder.
2. Run the command `git clone git@gitlab.com:william_w/iceandfire.git` to clone the repository.

## Test and Run:

### Initialize your project
1. Open your project folder on your preferred terminal.
2. Run the command `yarn` to install the project dependencies.

    
### Running the project

1.  Run the command `yarn test` to execute all the unit tests.
2.  On your terminal, run the command `yarn start` to launch the application; this will load the application on `http://localhost:3000`
3.  Start your preferred browser and open the link above.
4.  Use the navigation links on the left panel to visit the `Books`, `Houses` and `Characters` views.